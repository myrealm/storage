{
  inet = {
    inet = {
      desc = "This is the Internet";
    };
  };

  ls = {
    wg = {
      desc = "private wireguard network";
      domain = "wg.living-systems.dev";

      port = 51823; # wgport
      publicKey = "jOlAQuJnB+HhII9AMUFUmfjXu7G1J2WLEsdFhcpBJl8=";
      endpointIp = "116.203.106.198";

      # TODO
      # machines.<wg-server-machine>.ls.wg.ip
      # machines.<wg-server-machine>.ls.wg.ip6
      #
      ip = "10.100.0.1"; # monitoring for prometheus exporter
      ips = [ "10.100.0.1/24" "2a01:4f8:c2c:3bf1::1"]; # for wg server
    };

    k21 = {
      desc = "there's no place like LAN";
      domain = "lan";
      #domain = "lan.living-systems.dev";

      dhcpServer = {
        gateway = "192.168.0.1";
        router = "192.168.0.1";
        dnsServers = [
      # machines.nanopi.home.lan.ip
          "192.168.0.5"
      # machines.rpi.home.lan.ip
          "192.168.0.6"
        ];
        dhcpStart = "192.168.0.111";
        dhcpStop = "192.168.0.222";
      };
    };

    wifi = {
      desc = "UniFi WiFi Nano whatevs";
      domain = "lan";
      #domain = "lan.living-systems.dev";
      cfg = wifis.wifi;

      # TODO
      #dhcpServer = k21.dhcpServer;
      #dnsServer = k21.dnsServer;
    };

    slowfi = {
      desc = "Slow Client WiFi hosted on rpi3";
      domain = "slowfi";
      #domain = "slowfi.living-systems.dev";
      cfg = wifis.slowfi;

      # TODO
      #dhcpServer = k21.dhcpServer;
      #dnsServer = k21.dnsServer;
      prefix = "192.168.1.0";
      length = 24;
      cidr = "192.168.1.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.1.255";

      gateway = "192.168.1.1";
      dnsServers = [ "192.168.1.1" ];

      dhcpStart = "192.168.1.111";
      dhcpStop = "192.168.1.222";
      router = "192.168.0.6";
    };

    nanolan = {
      desc = "LAN behind nanopi's LAN port";
      domain = "nanolan";
      #domain = "nanolan.living-systems.dev";

      prefix = "192.168.2.0";
      length = 24;
      cidr = "192.168.2.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.2.255";

      gateway = "192.168.2.1";
      dnsServers = [ "192.168.2.1" ];

      dhcpStart = "192.168.2.111";
      dhcpStop = "192.168.2.222";
      router = "192.168.0.5";
    };

    private = {
      desc = "private ipv4 subnet";
      domain = "zzz";
      prefix = "172.18.0.0";
      length = 24;
      cidr = "172.18.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "172.18.0.255";
      router = "172.18.0.1";
    };
  };
}
    publicNetPath = ["ls" "k21"];
    privateNetPath = ["ls" "wg"];
    m = machines."${config.myrealm.layer.machine}";
    MAP = path: attrByPath path null m;

    #l = layers.ls;
    #LAP = path: getAttrFromPath path l;

    machine = config.myrealm.layer.machine;

    backend = machines.nanopi.ls.wg.ip;
    k21 = machines.nanopi.ls.k21.ip;

    l.service = {
      loki.host = "nanopi";
      loki.ip = backend;
      loki.port = 3100;

      postgres.host = "nanopi";
      postgres.metrics.ip = backend;
      postgres.metrics.port = 9187;

      prometheus.host = "nanopi";
      prometheus.ip = backend;
      prometheus.port = 9090;

      grafana.host = "nanopi";
      grafana.ip = k21;
      grafana.port = 3000;

      frontpage.host = "nanopi";
      frontpage.ip = k21;
      frontpage.port = 80;
    };

    l.metrics = {
      promtail = {
        ip = l.service.loki.ip;
        port =  l.service.loki.port;
      };

      node-exporter = {
        port =  9100;
        ip = MAP (privateNetPath ++ ["ip"]);
      };
    };
