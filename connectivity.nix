{
  inet = {
    inet = {
      to = [ ];
    };
  };

  home = {
    lan = {
      to = [ [ "inet" "inet"] [ "home" "wifi" ] [ "home" "slowfi" ] [ "home" "nanolan" ] ];
    };

    wifi = {
      to = [ [ "inet" "inet"] [ "home" "lan" ] [ "home" "slowfi" ] [ "home" "nanolan" ] ];
    };

    slowfi = {
      to = [ [ "inet" "inet"] [ "home" "lan" ] [ "home" "wifi" ] [ "home" "nanolan" ] ];
    };

    nanolan = {
      to = [ [ "inet" "inet"] [ "home" "lan" ] [ "home" "wifi" ] [ "home" "slowfi" ] ];
    };
  };

  sofia = {
    wg-sofia = {
      to = [ [ "inet" "inet"] [ "sofia" "private" ] ];
    };
    private = {
      to = [ [ "inet" "inet"] [ "sofia" "wg-sofia" ] ];
    };
  };

  tsti = {
    wg-vogo = {
      to = [ [ "inet" "inet"] ];
    };
  };

  ipc = {
    vpn = {
      to = [ [ "inet" "inet"] ];
    };
  };
}
