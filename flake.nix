{
  description = "A vars as flake module outputs";

  outputs = all@{ self, flake-utils,  ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" "aarch64-linux" ] (system: rec {

      apps = {
        graph = {
          type = "app";
        };
      };

    # modules and templates are all sytem agnostic
    }) // {

    # Default module, for use in dependent flakes
    #nixosModule = { config, ... }: { options = {}; config = {}; };

    # Same idea as nixosModule but a list or attrset of them.
    #nixosModules = { exampleModule = self.nixosModule; };

    nixosModules = {
      machines = import ./machines.nix;
      networks = import ./networks.nix;
      connectivity = import ./connectivity.nix;
    };

    # Utilized by `nix flake init -t <flake>`
    templates.default = {
      path = ./.;
      description = "all of this";
    };

    # Utilized by `nix flake init -t <flake>#<name>`
    templates.example = self.templates.default;
  };
}
