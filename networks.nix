let
  #wifis = import /root/secrets/wifi/wifis.nix;
  wifis = import /etc/nixos/secrets/wifis.nix;
in
{
  inet = {
    inet = {
      desc = "This is the Internet";
    };
  };

  ls = {
    wg = {
      desc = "private wireguard network";
      domain = "wg.living-systems.dev";

      port = 51823; # wgport
      publicKey = "jOlAQuJnB+HhII9AMUFUmfjXu7G1J2WLEsdFhcpBJl8=";
      endpointIp = "116.203.106.198";

      prefix = "10.100.0.0";
      length = 24;
      cidr = "10.100.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "10.100.0.255";
      router = "10.100.0.1";
      ip = "10.100.0.1"; # monitoring for prometheus exporter
      ips = [ "10.100.0.1/24" "2a01:4f8:c2c:3bf1::1"]; # for wg server
    };

    k21 = {
      desc = "there's no place like LAN";
      domain = "lan";
      #domain = "lan.living-systems.dev";

      prefix = "192.168.0.0";
      length = 24;
      cidr = "192.168.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.0.255";

      gateway = "192.168.0.1";
      dnsServers = [
        "192.168.0.7"
        "192.168.0.6"
      ];

      dhcpStart = "192.168.0.111";
      dhcpStop = "192.168.0.222";
      # TODO
      # dhcpServer
      router = "192.168.0.1";
    };

    wifi = {
      desc = "UniFi WiFi Nano whatevs";
      domain = "lan";
      #domain = "lan.living-systems.dev";
      cfg = wifis.wifi;

      prefix = "192.168.0.0";
      length = 24;
      cidr = "192.168.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.0.255";

      gateway = "192.168.0.1";
      dnsServers = [
        "192.168.0.7"
        "192.168.0.6"
      ];

      dhcpStart = "192.168.0.111";
      dhcpStop = "192.168.0.222";
      router = "192.168.0.1";
    };

    slowfi = {
      desc = "Slow Client WiFi hosted on rpi3";
      domain = "slowfi";
      #domain = "slowfi.living-systems.dev";
      cfg = wifis.slowfi;

      prefix = "192.168.1.0";
      length = 24;
      cidr = "192.168.1.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.1.255";

      gateway = "192.168.1.1";
      dnsServers = [ "192.168.1.1" ];

      dhcpStart = "192.168.1.111";
      dhcpStop = "192.168.1.222";
      router = "192.168.0.6";
    };

    nanolan = {
      desc = "LAN behind nanopi's LAN port";
      domain = "nanolan";
      #domain = "nanolan.living-systems.dev";

      prefix = "192.168.2.0";
      length = 24;
      cidr = "192.168.2.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.2.255";

      gateway = "192.168.2.1";
      dnsServers = [ "192.168.2.1" ];

      dhcpStart = "192.168.2.111";
      dhcpStop = "192.168.2.222";
      router = "192.168.0.7";
    };

    private = {
      desc = "private ipv4 subnet";
      domain = "zzz";
      prefix = "172.18.0.0";
      length = 24;
      cidr = "172.18.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "172.18.0.255";
      router = "172.18.0.1";
    };
  };

  home = {
    lan = {
      desc = "there's no place like LAN";
      domain = "lan";

      prefix = "192.168.0.0";
      length = 24;
      cidr = "192.168.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.0.255";

      gateway = "192.168.0.1";
      dnsServers = [
        "192.168.0.7"
        "192.168.0.6"
      ];

      dhcpStart = "192.168.0.111";
      dhcpStop = "192.168.0.222";
      # TODO
      # dhcpServer
      router = "192.168.0.1";
    };

    wifi = {
      desc = "UniFi WiFi Nano whatevs";
      domain = "lan";
      cfg = wifis.wifi;

      prefix = "192.168.0.0";
      length = 24;
      cidr = "192.168.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.0.255";

      gateway = "192.168.0.1";
      dnsServers = [
        "192.168.0.7"
        "192.168.0.6"
      ];

      dhcpStart = "192.168.0.111";
      dhcpStop = "192.168.0.222";
      router = "192.168.0.1";
    };

    slowfi = {
      desc = "Slow Client WiFi hosted on rpi3";
      domain = "slowfi";
      cfg = wifis.slowfi;

      prefix = "192.168.1.0";
      length = 24;
      cidr = "192.168.1.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.1.255";

      gateway = "192.168.1.1";
      dnsServers = [ "192.168.1.1" ];

      dhcpStart = "192.168.1.111";
      dhcpStop = "192.168.1.222";
      router = "192.168.0.6";
    };

    nanolan = {
      desc = "LAN behind nanopi's LAN port";
      domain = "nanolan";

      prefix = "192.168.2.0";
      length = 24;
      cidr = "192.168.2.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.2.255";

      gateway = "192.168.2.1";
      dnsServers = [ "192.168.2.1" ];

      dhcpStart = "192.168.2.111";
      dhcpStop = "192.168.2.222";
      router = "192.168.0.7";
    };
  };

  sofia = {
    wg = let wgport = 41194; in {
      desc = "Drahtschutznetzwerk auf sofia";
      domain = "cockb.org";

      port = wgport;
      publicKey = "ei/JIlLGtqSTV1OFsHkOx7aeLrI0lzXGkgK/G3Wt52k=";
      endpoint = "116.202.235.59:41194";
      endpointIp = "116.202.235.59";
      #endpoint = "116.202.235.59:${lib.toString 41194}"; # where lib?

      prefix = "192.168.43.0";
      length = 24;
      cidr = "192.168.43.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.43.255";
      router = "192.168.43.1";
    };
    private = {
      desc = "private ipv4 subnet";
      domain = "zzz";
      prefix = "172.18.0.0";
      length = 24;
      cidr = "172.18.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "172.18.0.255";
      router = "172.18.0.1";
    };
  };

  ipc = {
    vpn = {
      desc = "vpn";
      domain = "ipc.uni-tuebingen.de";
    };
  };

  tsti = {
    wg = let wgport = 51820; in {
      desc = "Drahtschutznetzwerk auf vogo";
      domain = "tsti";

      port = wgport;
      publicKey = "BcXiU2VyJ3Jwn56N18Jwv2P9g9qsCLbv6tMB9enkjE0=";
      endpoint = "134.2.220.33:51820";
      endpointIp = "134.2.220.33";

      prefix = "10.1.1.0";
      length = 24;
      cidr = "10.1.1.0/24";
      netmask = "255.255.255.0";
      broadcast = "10.1.1.255";
      router = "10.1.1.1";
    };
  };
}
